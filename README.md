## Récupérer les submodules

```
git submodule update --init --recursive
```

## Lancer les bases de données

```
    docker compose --project-name microservices up
```

## Définir le .env de agreclassesprofs

## Lancer les projets

## Importer dans vos postman le fichier json suivant

[lien vers le json postman](./documentation/college.postman_collection.json)

## Architecture

[architecture](./documentation/archi.drawio.png)
